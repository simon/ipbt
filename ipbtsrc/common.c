#include <stdio.h>

#include "putty.h"
#include "ipbt.h"

void ipbt_usage(void)
{
    fputs(usagemsg, stdout);
}

static const char licencemsg[] =
    "IPBT is an application derived from the PuTTY source base by Simon\n"
    "Tatham.\n"
    "\n"
    "The PuTTY copyright notice is reproduced below. The long list of\n"
    "copyright holders are not all actually relevant, since some of them\n"
    "contributed code to PuTTY which is not included in IPBT.\n"
    "\n"
    "| PuTTY is copyright Simon Tatham and many other contributors.\n"
    "\n"
    "Those portions of IPBT which are not part of PuTTY are copyright\n"
    "Simon Tatham and other contributors. The same licence terms apply\n"
    "to them as to PuTTY:\n"
    "\n"
    "| Permission is hereby granted, free of charge, to any person\n"
    "| obtaining a copy of this software and associated documentation files\n"
    "| (the \"Software\"), to deal in the Software without restriction,\n"
    "| including without limitation the rights to use, copy, modify, merge,\n"
    "| publish, distribute, sublicense, and/or sell copies of the Software,\n"
    "| and to permit persons to whom the Software is furnished to do so,\n"
    "| subject to the following conditions:\n"
    "|\n"
    "| The above copyright notice and this permission notice shall be\n"
    "| included in all copies or substantial portions of the Software.\n"
    "|\n"
    "| THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,\n"
    "| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\n"
    "| MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND\n"
    "| NONINFRINGEMENT.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE\n"
    "| FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF\n"
    "| CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION\n"
    "| WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\n"
    ;

void ipbt_licence(void)
{
    fputs(licencemsg, stdout);
}

void ipbt_version(void)
{
#ifdef PACKAGE_VERSION
    printf("ipbt version %s\n", PACKAGE_VERSION);
#else
    printf("ipbt, source commit %s\n", commitid);
#endif
}

/*
 * We're only _replaying_ terminal data, so we never need to send back
 * to a real application. Thus, ldisc_send and ldisc_echoedit_update
 * as called from terminal.c are stub functions.
 */
void ldisc_send(Ldisc *ldisc, const void *buf, int len, bool interactive) {}
void ldisc_echoedit_update(Ldisc *ldisc) {}
void ldisc_enable_prompt_callback(Ldisc *ldisc, prompts_t *prompts) {}
bool ldisc_has_input_buffered(Ldisc *ldisc) { return false; }
void ldisc_provide_userpass_le(Ldisc *ldisc, TermLineEditor *le) {}

void cleanup_exit(int code)
{
    exit(code);
}
void modalfatalbox(const char *p, ...)
{
    va_list ap;
    pre_exit();
    fprintf(stderr, "%s: ", appname);
    va_start(ap, p);
    vfprintf(stderr, p, ap);
    va_end(ap);
    fputc('\n', stderr);
    cleanup_exit(1);
}

const char *const typenames[NTYPES] = {
    #define FILETYPE_NAMEDEF(id, val, name) [id] = name,
    FILETYPES(FILETYPE_NAMEDEF)
    #undef FILETYPE_NAMEDEF
};

/*
 * Stub functions.
 */
void logflush(LogContext *handle) {}
void logtraffic(LogContext *handle, unsigned char c, int logmode) {}

/*
 * We don't save and load PuTTY configuration data, so these are
 * all stubs too.
 */
settings_w *open_settings_w(const char *sessionname, char **errmsg)
{ return NULL; }
void write_setting_s(settings_w *handle, const char *key, const char *value) {}
void write_setting_i(settings_w *handle, const char *key, int value) {}
void close_settings_w(settings_w *handle) {}
settings_r *open_settings_r(const char *sessionname)
{ return NULL; }
char *read_setting_s(settings_r *handle, const char *key)
{ return NULL; }
int read_setting_i(settings_r *handle, const char *key, int defvalue)
{ return defvalue; }
FontSpec *read_setting_fontspec(settings_r *handle, const char *name)
{ return NULL; }
Filename *read_setting_filename(settings_r *handle, const char *name)
{ return NULL; }
void write_setting_fontspec(settings_w *handle, const char *name,
                            FontSpec *value) {}
void write_setting_filename(settings_w *handle, const char *name,
                            Filename *value) {}
void close_settings_r(settings_r *handle) {}
settings_e *enum_settings_start(void) { return NULL; }
bool enum_settings_next(settings_e *handle, strbuf *out) { return false; }
FontSpec *platform_default_fontspec(const char *name)
{
    FontSpec *ret = snew(FontSpec);
    ret->name = dupstr("");
    return ret;
}
Filename *platform_default_filename(const char *name)
{
    Filename *ret = snew(Filename);
    ret->path = dupstr("");
    return ret;
}
char *platform_default_s(const char *name) { return NULL; }
int platform_default_i(const char *name, int def) { return def; }
bool platform_default_b(const char *name, bool def) { return def; }
void enum_settings_finish(settings_e *handle) {}
int default_port = -1, default_protocol = -1;

/*
 * Other miscellaneous stubs.
 */
void queue_toplevel_callback(toplevel_callback_fn_t fn, void *ctx) {}
void delete_callbacks_for_context(void *ctx) {}

/*
 * Stub functions for the TermWin vtable.
 */
bool ipbt_setup_draw_ctx(TermWin *tw) { return true; }
void ipbt_draw_trust_sigil(TermWin *tw, int x, int y) {}
void ipbt_free_draw_ctx(TermWin *tw) {}
void ipbt_set_cursor_pos(TermWin *tw, int x, int y) {}
void ipbt_set_raw_mouse_mode(TermWin *tw, bool enable) {}
void ipbt_set_raw_mouse_mode_pointer(TermWin *tw, bool enable) {}
void ipbt_set_scrollbar(TermWin *tw, int total, int start, int page) {}
void ipbt_bell(TermWin *tw, int mode) {}
void ipbt_clip_write(TermWin *tw, int clipid, wchar_t *text, int *attrs,
                     truecolour *colours, int len, bool deselect) {}
void ipbt_clip_request_paste(TermWin *tw, int clipboard) {}
void ipbt_refresh(TermWin *tw) {}
void ipbt_request_resize(TermWin *tw, int w, int h) {}
void ipbt_set_title(TermWin *tw, const char *title, int codepage) {}
void ipbt_set_icon_title(TermWin *tw, const char *icontitle,
                         int codepage) {}
void ipbt_set_minimised(TermWin *tw, bool minimised) {}
void ipbt_set_maximised(TermWin *tw, bool maximised) {}
void ipbt_move(TermWin *tw, int x, int y) {}
void ipbt_set_zorder(TermWin *tw, bool top) {}
void ipbt_palette_set(TermWin *tw, unsigned start, unsigned ncolours,
                      const rgb *colours) {}
void ipbt_palette_get_overrides(TermWin *tw, Terminal *term) {}
int ipbt_char_width(TermWin *tw, int uc) { return 1; }
void ipbt_draw_cursor(TermWin *tw, int x, int y, wchar_t *text, int len,
                      unsigned long attr, int lattr, truecolour tc)
{
    ipbt_draw_text(tw, x, y, text, len, attr, lattr, tc);
}

