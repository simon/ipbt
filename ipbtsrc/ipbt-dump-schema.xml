<?xml version="1.0"?>
<xs:schema
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    targetNamespace="https://www.chiark.greenend.org.uk/~sgtatham/ipbt/ttyrecording"
    xmlns="https://www.chiark.greenend.org.uk/~sgtatham/ipbt/ttyrecording"
    elementFormDefault="qualified">

<!-- Top-level element is <ttyrecording>, containing nothing but a
     sequence of <frame>. -->
<xs:element name="ttyrecording">
  <xs:complexType>
    <xs:sequence>

      <!-- <frame> contains a sequence of <row>, followed by a <cursor>. -->
      <xs:element name="frame" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>

            <!-- <row> contains a sequence of <t> indicating spans of
                 text within the row. -->
            <xs:element name="row" minOccurs="1" maxOccurs="unbounded">
              <xs:complexType>
                <xs:sequence>

                  <!-- Each <t> has a ton of attributes for the text
                       within this span, but they're all optional. -->
                  <xs:element name="t" minOccurs="1" maxOccurs="unbounded">
                    <xs:complexType>
                      <xs:simpleContent>
                        <xs:extension base="xs:string">
                          <!-- SGR attributes set via ESC [ ... m -->
                          <xs:attribute name="bold" type="xs:boolean"/>
                          <xs:attribute name="underline" type="xs:boolean"/>
                          <xs:attribute name="reverse" type="xs:boolean"/>
                          <xs:attribute name="blink" type="xs:boolean"/>
                          <xs:attribute name="dim" type="xs:boolean"/>
                          <xs:attribute name="strike" type="xs:boolean"/>
                          <xs:attribute name="fg" type="colour"/>
                          <xs:attribute name="bg" type="colour"/>

                          <!-- Attributes describing the Unicode
                               nature of the text. These allow a
                               reader to determine character widths
                               without needing the Unicode database.

                               If "wide" is true, printing characters
                               in this span occupy two terminal
                               character cells each, such as CJK
                               characters or most emoji. If "wide" is
                               false, printing characters in this span
                               occupy one cell each. No span contains
                               both widths of character.

                               If "combined" is true, then the text in
                               this span contains exactly one printing
                               character, followed by nothing but
                               combining characters. So it occupies a
                               single character cell if wide=false, or
                               two if wide=true. If "combined" is
                               false, no combining characters are
                               present in the span, and every Unicode
                               code point corresponds to a printing
                               character. -->
                          <xs:attribute name="wide" type="xs:boolean"/>
                          <xs:attribute name="combined" type="xs:boolean"/>
                        </xs:extension>
                      </xs:simpleContent>
                    </xs:complexType>
                  </xs:element>
                </xs:sequence>

                <!-- <row> has one optional attribute, "type", which
                     indicates terminal attributes that apply to the
                     whole line, like the ESC # 3 system for
                     double-width and double-height. -->
                <xs:attribute name="type" type="lineAttribute"/>
              </xs:complexType>
            </xs:element>

            <!-- <cursor> indicates the state of the cursor. It's
                 empty, and only has attributes: whether the cursor is
                 hidden or visible, and where it is.

                 ipbt-dump currently won't mention the location of the
                 cursor if it isn't visible. But I'm ruling here that
                 if another producer did, it would be legal.

                 I'd like to add a restriction here that if the cursor
                 _is_ stated as visible, you must include the x and y
                 attributes. FIXME. -->
            <xs:element name="cursor" minOccurs="1" maxOccurs="1">
              <xs:complexType>
                <xs:attribute name="state" type="cursorState" use="required"/>
                <xs:attribute name="x" type="xs:nonNegativeInteger"/>
                <xs:attribute name="y" type="xs:nonNegativeInteger"/>
              </xs:complexType>
            </xs:element>
          </xs:sequence>

          <!-- <frame> has two required attributes: the index of this
               frame within the file, and its byte offset within the file. -->
          <xs:attribute name="index" type="xs:nonNegativeInteger"
                        use="required"/>
          <xs:attribute name="fileoffset" type="xs:nonNegativeInteger"
                        use="required"/>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
</xs:element>

<!-- Legal values of the 'type' attribute on <row> -->
<xs:simpleType name="lineAttribute">
  <xs:restriction base="xs:string">
    <xs:enumeration value="wide"/> <!-- single-height, double-width (ESC#6) -->
    <xs:enumeration value="top"/> <!-- double-size top half (ESC#3) -->
    <xs:enumeration value="bot"/> <!-- double-size bottom half (ESC#4) -->
  </xs:restriction>
</xs:simpleType>

<!-- Legal values of the 'state' attribute on <cursor> -->
<xs:simpleType name="cursorState">
  <xs:restriction base="xs:string">
    <xs:enumeration value="hidden"/>
    <xs:enumeration value="visible"/>
  </xs:restriction>
</xs:simpleType>

<!-- Legal values of the 'fg' and 'bg' attributes on <t>.

     These fall into several categories:

     The eight ordinary ANSI colours are given by name:
      - black
      - red, green, blue
      - cyan, magenta, yellow
      - white

     Bold versions of those colours are prefixed with 'bold-', e.g.
     'bold-yellow'. (This is semantically different from yellow with
     the SGR 1 bold attribute: <t fg="yellow" bold="1"> is not the
     same thing as <t fg="bold-yellow">.) These colours are found at
     indices 8-15 in the xterm 256-colour system, and some SCO escape
     sequences can request them too.

     The xterm 256-colour system also includes a low-resolution RGB
     cube with six possible values per channel. These are listed as
     xterm-rgb-000 to xterm-rgb-555, where the final three digits are
     R,G,B respectively and each takes a value in {0,...,5}.

     The xterm 256-colour system also includes a sequence of grey
     shades with 26 levels in total, counting black and white. The
     ones that are not black and white are encoded as xterm-grey-1 to
     xterm-grey-24.

     Finally, xterm true colour escape sequences can be used to select
     an arbitrary 24-bit RGB triple. Those are indexed in the familiar
     form from HTML and CSS of a '#' followed by six hex digits in
     rrggbb order.
-->
<xs:simpleType name="colour">
  <xs:restriction base="xs:string">
    <xs:pattern value="#[0-9a-fA-F]{6}|xterm-rgb-[0-5]{3}|xterm-grey-([0-9]|1[0-9]|2[0-4])|(bold-)?(black|red|green|yellow|blue|cyan|magenta|white)"/>
  </xs:restriction>
</xs:simpleType>

</xs:schema>
