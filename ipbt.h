#define FILETYPES(X)                    \
    X(TTYREC, 0, "ttyrec")             \
    X(NHRECORDER, 1, "nh-recorder") \
    X(RAWBYTES, 2, "rawbytes") \
    /* end of list */

enum {
    #define FILETYPE_ENUM(id, val, name) id = val,
    FILETYPES(FILETYPE_ENUM)
    #undef FILETYPE_ENUM

    NTYPES,

    /* Number of file types that can be auto-detected, which is 1 less
     * because of RAWBYTES */
    NTYPES_AUTODETECT = NTYPES - 1,

    /* Extra value used in the variable for _default_ file type,
     * indicating that we should throw an error if the file type can't
     * be inferred */
    NODEFAULT = -1
};

extern const char *const typenames[NTYPES];

unsigned long long read_tty_recording(
    const char *name, int deftype, Terminal *term, FILE *reportfp,
    bool first_file,
    void (*store_frame)(void *ctx, unsigned long long delay, long fileoff,
                        strbuf *termdata),
    void *store_frame_ctx);

void ipbt_usage(void);
void ipbt_licence(void);
void ipbt_version(void);

/* Things that vary between ipbt and ipbt-dump */
extern const char usagemsg[];
void pre_exit(void);

/* Stubs for the TermWin vtable */
bool ipbt_setup_draw_ctx(TermWin *tw);
void ipbt_draw_trust_sigil(TermWin *tw, int x, int y);
void ipbt_free_draw_ctx(TermWin *tw);
void ipbt_set_cursor_pos(TermWin *tw, int x, int y);
void ipbt_set_raw_mouse_mode(TermWin *tw, bool enable);
void ipbt_set_raw_mouse_mode_pointer(TermWin *tw, bool enable);
void ipbt_set_scrollbar(TermWin *tw, int total, int start, int page);
void ipbt_bell(TermWin *tw, int mode);
void ipbt_clip_write(TermWin *tw, int clipid, wchar_t *text, int *attrs,
                     truecolour *colours, int len, bool deselect);
void ipbt_clip_request_paste(TermWin *tw, int clipboard);
void ipbt_refresh(TermWin *tw);
void ipbt_request_resize(TermWin *tw, int w, int h);
void ipbt_set_title(TermWin *tw, const char *title, int codepage);
void ipbt_set_icon_title(TermWin *tw, const char *icontitle,
                         int codepage);
void ipbt_set_minimised(TermWin *tw, bool minimised);
void ipbt_set_maximised(TermWin *tw, bool maximised);
void ipbt_move(TermWin *tw, int x, int y);
void ipbt_set_zorder(TermWin *tw, bool top);
void ipbt_palette_set(TermWin *tw, unsigned start, unsigned ncolours,
                      const rgb *colours);
void ipbt_palette_get_overrides(TermWin *tw, Terminal *term);
int ipbt_char_width(TermWin *tw, int uc);
void ipbt_draw_cursor(TermWin *tw, int x, int y, wchar_t *text, int len,
                      unsigned long attr, int lattr, truecolour tc);
/* But this isn't a stub; main programs must implement it. */
void ipbt_draw_text(TermWin *tw, int x, int y, wchar_t *text, int len,
                    unsigned long attr, int lattr, truecolour tc);
